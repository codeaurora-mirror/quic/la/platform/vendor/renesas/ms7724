The "ms7724" product defines a target 'renesas/r0p7724'
without a kernel or bootloader.

It can be used to build the entire user-level system.

