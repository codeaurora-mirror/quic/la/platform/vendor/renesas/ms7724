#!/system/bin/sh

# start network setting
ifconfig eth0
case $? in
    255) netcfg eth0 dhcp ;;
esac

# disable boot animation for a faster boot sequence when needed
boot_anim=`getprop ro.kernel.android.bootanim`
case "$boot_anim" in
    0)  setprop debug.sf.nobootanimation 1
    ;;
esac
